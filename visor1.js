function initMap(){

    var mymap = L.map('MyMap').setView([4.600116, -74.190690], 18);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWptdXJjaWEiLCJhIjoiY2tjcXJ5NmsxMGdndzMybzc1bDZ5amw2bCJ9.saZkLq3zUUsG8jCaNfquWA'
}).addTo(mymap);

    var marker = L.marker([4.658084, -74.093334]).addTo(mymap); //Parque Simon Bolivar
    var marker = L.marker([4.663740, -74.087149]).addTo(mymap); //Parque El Salitre
    var marker = L.marker([4.597706, -74.081355]).addTo(mymap); //Parque Metropolitano Tercer Milenio
    var marker = L.marker([4.632261, -74.129120]).addTo(mymap); //Panaderia La Marsellesa
    var marker = L.marker([4.634342, -74.127912]).addTo(mymap); //Empanadas Donde Charles y punto
    
    var polygon = L.polygon([
        [4.658084, -74.093334],
        [4.663740, -74.087149],
        [4.634342, -74.127912],
        [4.632261, -74.129120],
        [4.597706, -74.081355]
    ]).addTo(mymap);

}